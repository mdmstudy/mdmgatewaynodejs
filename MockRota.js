
function criarEntregas(totalEntregas){
  var aux = {};
  var arrayRetorno = [totalEntregas];
  for(i=0;i<totalEntregas-1;i++){
    aux =
         {
                  "CodigoEntrega":54777088,
                  "NomeCliente":"0000065 - COML LUIZ CLAUDIO",
                  "EnderecoCliente":"R AMAZONAS,002691",
                  "CodigoClienteNegocio":65,
                  "PontoInteresse":{
                     "Latitude":-26.9420058801137,
                     "Longitude":-49.0703373421026,
                     "RaioAderencia":100
                  },
                  "Instrucao":"",
                  "ValorEntrega":"4692.62",
                  "SequenciaPrevista":1,
                  "ItemDevolvido":"False",
                  "CodigoMotivoDevolucao":0,
                  "TokenValidacao":"54777088",
                  "AtivarPesquisaEntrega":false,
                  "StatusDevolucao":0,
                  "InicioJanelaEntrega":"07:10",
                  "FimJanelaEntrega":"17:00",
                  "TempoAtendimento":0,
                  "KmPrevisto":0,
                  "TaxaDescarga":null,
                  "NotasFiscais":[
                     {
                        "CodigoNF":81940929,
                        "NumeroNF":"0968894-001 72946595",
                        "ValorNF":"2242.82",
                        "TipoNF":"VENDA",
                        "ItemDevolvido":"False",
                        "CodigoMotivoDevolucaoNF":0,
                        "StatusDevolucaoNF":0,
                        "ProporcionalCaixa":15.315000,
                        "ItensNF":[
                           {
                              "CodigoItemNF":284574597,
                              "DescricaoItemNF":"SK LT350SH18NPA",
                              "QuantidadeItemNF":"45",
                              "UnidadeItemNF":"cx  ",
                              "ItemDevolvido":"False",
                              "CodigoMotivoDevolucaoItemNF":0,
                              "StatusDevolucaoItemNF":0
                           }
                        ]
                     }]
                   };
        aux.CodigoEntrega = i;
        aux.NomeCliente = i + aux.NomeCliente;
        aux.NotasFiscais[i].CodigoNF = i;
        aux.NotasFiscais[i].ItensNF = [];
        arrayRetorno[i] = aux;

  }
  return arrayRetorno;
}

MockRota = {
  getListaListaMotivoDevolucao : function(){
    var obj = {
                  codigo:1,
                  descricao: "Motivo Teste"};
    var lista = [obj];
    return JSON.stringify(lista);
  },
  getListaMotivoNaoRecolha : function(){
    var obj = {
                  codigo:1,
                  descricao: "Motivo Teste"};
    var lista = [obj];
    return JSON.stringify(lista);
  },
  getListaMotivoPNP : function(){
    var obj = {
                  codigoMotivo:1,
                  descricao: "Motivo Teste",
                  cdTipocategoria: 1};
    var lista = [obj];
    return JSON.stringify(lista);
  },
  getListaCategoriaPDVCritico : function(){
    var categ = {
                  cdCategoriaPdvCritico:1,
                  descricao: "Motivo Teste",
                  cdTipocategoria: 1};
    var lista = [categ];
    return JSON.stringify(lista);
  },
  getListaMotivoEstadia : function(){
    var motivo = {
                  codigoMotivo:1,
                  descricao: "Motivo Teste",
                  MotivoCliente: false};
    var lista = [motivo];
    return JSON.stringify(lista);
  },
 getRota: function(){
   var retorno =  { "CodigoRota":4114309,
      "CodigoRotaNegocio":149713,
      "ImportadaFoxtrot":true,
      "RetornoMpd":true,
      "InicioPrevisao":"02/16/2018 16:17:09",
      "TempoPrevisto":"",
      "ValorPrevisao":"19598.05",
      "Motorista":"CARLOS CESAR DE VARGAS",
      "CodigoMotorista":1278,
      "Ajudante1":"",
      "Ajudante2":"",
      "Odometro":111107208,
      "PermiteDevolucaoParcialNF":"True",
      "PermiteDevolucaoParcialItemNF":"True",
      "Entregas":criarEntregas(2),
                     "HabilitarIntegracaoFoxtrot":true,
                        "ChaveCDDFoxtrot":"13136667-fbda-4a2c-8711-c6bb85056fbc",
                        "HabilitarSugestoesEntrega":true,
                        "UtilizarBlitz":false,
                        "UtilizarAvaliacaoPDV":false,
                        "TipoSegmento":"AS",
                        "TipoFrota":"Padronizado",
                        "UtilizarBonusAs":true,
                        "PermiteTaxaDescarga":true,
                        "PermiteEstadia":true,
                        "SequenciaAleatoriaBlitz":false,
                        "ValorDiariaHospedagem":0,
                        "ValorPernoiteVeiculo":0,
                        "ValorRefeicao":0,
                        "InicioEstadia":null
                     };
                return JSON.stringify(retorno);


}
}
