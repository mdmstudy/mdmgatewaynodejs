

# README #

Passos para setup e deploy do projeto

### Oquê ele faz? ###

* Adicionar uma camada de modularização no acesso ao serviço de comunicação entre o EntregaFácil e a Aplicação Web do MDM
![enter image description here](blueprint.png)
```mermaid
graph TD
A[SERVIDOR] --- B(Gateway)
B(Gateway)  --- C[EntregaFácil]

subgraph Gateway
B(Gateway)  -.- E[Mock]
B(Gateway)  --- D[MongoDB]
end
```



### Sumário ###

 1. Baixando projeto
 2. Dependencias
 3. Configuração
 5. Rodando o projeto

### Baixando o projeto ###

    git clone https://natanloterio@bitbucket.org/mdmstudy/mdmgatewaynodejs.git

### Dependências ###
Depois de clonar o projeto, será preciso baixar as dependências. Para isso, abra um terminal e vá até a pasta raiz do projeto. Ex.:

    >cd C:\Projetos\GatewayEntregaFacil\MDMGatewayNodeJS
    >npm install  

### Configuração ###

####  1.  Servidor ####
A configuração do servidor deve ser feita no arquivo `config-mdm.js`
		
    configMDM = {
        serverMDM: 'http://186.250.185.139:5301'
      };

 ####  2. Mongo  ####
 A configuração do servidor deve ser feita no arquivo `config-mdm.js`
		
    configMDM = {
        serverMongo: 'mongodb://root:root@ds011775.mlab.com:11775/natan'
      };
 ####  3. Mock  ####
 A configuração dos dados de mock devem ser feitas no arquivo  `MockRota.js`que já está importado no arquivo `index.js`. 
 
 #####  'Mockando' um endpoint  #####
 Para mockar um endpoint, basta escrever como abaixo:

     app.get('/WS/Servico.svc/MobileWeb/BuscarListaMotivoDevolucao', (req, res, next) => {
      res.end(MockRota.getListaListaMotivoDevolucao());
    })

### Rodando o projeto ###
Depois de tudo configurado, basta executar:

    node index.js
Ou se quiser depurar usando o Chrome, basta rodar com o parâmetro "inspect"

`node --inspect index.js`


----------

