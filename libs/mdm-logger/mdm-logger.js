const Transport = require('winston-transport');
const util = require('util');
const common = require('./commons.js')
const os = require('os');
const uuidv1 = require('uuid/v1');

class MDMLoggerMongo extends Transport {

  constructor(opts) {
    super(opts);
    this._dbo = null;
    this._COLLECTION = "events";
    var MongoClient = require('mongodb').MongoClient;
    var url = "mongodb://root:root@ds011775.mlab.com:11775/natan";
    var options = {};

    MongoClient.connect(url, (err, db) => {
      if (err) throw err;
      console.log("Database created!");
      this._dbo = db.db("natan");
      this._dbo.createCollection(this._COLLECTION, function(err, res) {
        if (err) throw err;
        console.log("Collection created!");
      });
    });

    this.json        = options.json !== false;
    this.logstash    = options.logstash    || false;
    this.colorize    = options.colorize    || false;
    this.maxsize     = options.maxsize     || null;
    this.rotationFormat = options.rotationFormat || false;
    this.zippedArchive = options.zippedArchive || false;
    this.maxFiles    = options.maxFiles    || null;
    this.prettyPrint = options.prettyPrint || false;
    this.label       = options.label       || null;
    this.timestamp   = options.timestamp != null ? options.timestamp : true;
    this.eol         = options.eol || os.EOL;
    this.tailable    = options.tailable    || false;
    this.depth       = options.depth       || null;
    this.showLevel   = options.showLevel === undefined ? true : options.showLevel;
    this.maxRetries  = options.maxRetries || 2;

  }

  saveOnMongo(doc){

      doc._id = uuidv1();
      this._dbo.collection(this._COLLECTION).insertOne(doc, function(err, res) {
        if (err) throw err;
        console.log("request salvo!");
      });

  }


  log(level, msg, meta, callback) {

    if (typeof msg !== 'string') {
      msg = '' + msg;
    }

    var output = common.log({
      level:       level,
      message:     msg,
      meta:        meta,
      json:        this.json,
      logstash:    this.logstash,
      colorize:    this.colorize,
      prettyPrint: this.prettyPrint,
      timestamp:   this.timestamp,
      showLevel:   this.showLevel,
      stringify:   this.stringify,
      label:       this.label,
      depth:       this.depth,
      formatter:   this.formatter
    });

    output = JSON.parse(output);

    if (typeof output === 'string') {
      output += this.eol;
    }

    this.saveOnMongo(output)
    console.log(output)
  }

};


module.exports = MDMLoggerMongo;
