const c =  require('./config-mdm');
const configMDM = c;
const express = require('express')
const MDMLogger = require('./libs/mdm-logger/mdm-logger.js')
const timeout = require('connect-timeout');
const logger = require('bunyan-request-logger')
const httpProxy = require('express-http-proxy')
const volley = require('volleyball')
const winston = require('winston')
const expressWinston = require('winston-express-middleware')
const userServiceProxy = httpProxy(configMDM.c.serverMDM)
const MDMLoggerMongo = new MDMLogger(configMDM.c.serverMongo)
const app = express()
const mockRota = require('./MockRota.js')
const PORT = 3005;
const bodyParser = require('body-parser');
var ip = require('ip');

var count = 0;

// volleyball
app.use(volley)

/*
faz o 'parse' do body do request do express
no item @L1 você pode ver isso. Ele vai adicionar
no parametro 'res' do callback
*/
app.use(bodyParser());
app.use(timeout(120000));
app.disable('etag');
/*
app.use(expressWinston.logger({
  transports: [
    new winston.transports.Console({
      json: true,
      colorize: true
    }),
    new winston.transports.File({ filename: 'combined.log' }),
    new MDMLoggerMongo()
  ],
  meta: true, // optional: control whether you want to log the meta data about the request (default to true)
  msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
  expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
  colorize: true, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
  ignoreRoute: function (req, res) { return false; } // optional: allows to skip some log messages based on request and/or response
}));*/

expressWinston.requestWhitelist.push('body')
expressWinston.responseWhitelist.push('body')

// Os endpoints abaixo que foram mapeados, servem para 'Mockar' a resposta obtida
/*
app.get('/WS/Servico.svc/MobileWeb/BuscarListaMotivoDevolucao', (req, res, next) => {
  res.end(MockRota.getListaListaMotivoDevolucao());
})

app.get('/WS/Servico.svc/MobileWeb/BuscarListaMotivoPnp', (req, res, next) => {
  res.end(MockRota.getListaMotivoPNP());
})

app.get('/WS/Servico.svc/MobileWeb/BuscarMotivosNaoRecolha', (req, res, next) => {
  res.end(MockRota.getListaMotivoNaoRecolha());
})

app.get('/WS/Servico.svc/MobileWeb/BuscarListaMotivoEstadia', (req, res, next) => {
  res.end(MockRota.getListaMotivoEstadia())
})

app.get('/WS/Servico.svc/MobileWeb/BuscarCategoriasPdvCritico', (req, res, next) => {
  res.end(MockRota.getListaCategoriaPDVCritico())
})


app.get('/WS/Servico.svc/MobileWeb/BuscarRotaComDataInicio', (req, res, next) => {
  res.end(MockRota.getRota())
})
*/

/*
 [I1]Interceptador dos principais requests
*/
app.all('*', (req, res, next) => {
  if(req.method == "POST" && req.body.Comando && req.body.Comando.Conteudo.includes("Tipo\":21")){
    console.log("Recebendo ocorrência Anomalia!");
  }
  userServiceProxy(req, res, next)
})


app.listen(PORT);
console.log("Servidor Rodando "+ip.address() +":"+PORT);
